import javax.swing.*;
import java.awt.event.*;
public class FormularioC extends JFrame implements ActionListener{
    private JButton boton1,boton2;
    public FormularioC() {
        setLayout(null);
        boton1=new JButton("Var�n");
        boton1.setBounds(10,10,100,30);
        boton1.addActionListener(this);
        add(boton1);
        boton2=new JButton("Mujer");
        boton2.setBounds(10,70,100,30);
        boton2.addActionListener(this);
        add(boton2);
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource()==boton1) {
            setTitle("Var�n");
        }
        if (e.getSource()==boton2) {
            setTitle("Mujer");
        }
    }
    
    public static void main(String[] ar) {
        FormularioC formulario1=new FormularioC();
        formulario1.setBounds(0,0,130,140);
        formulario1.setVisible(true);
    }
}